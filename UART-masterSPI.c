/* ================================================
 * File Path     : uart-masterSPI.c
 * Author        : F.MORELLE
 * Date          : 26/03/22
 * Copyright     : (c) 2022 F.MORELLE
 *-------------------------------------------------
 *
 * SamBOT
 * carte maitre
 *
 * SAMboard v3 / Launchpad 1.5 / MSP-EXP430G2ET2553
 *-------------------------------------------------
 */

#include <msp430g2553.h>
#include <string.h>

#define CMDLEN  12               // longueur maximum de la commande utilisateur
#define LF      0x0A
#define CR      0x0D
#define BS      0x7F

unsigned char c;
char  cmd[CMDLEN];      // tableau de caractere lie a la commande user
unsigned int   nb_car;           // compteur nombre carateres saisis
unsigned int   p;
int etat;
char  query[6]= {0x3F, 0x3F, 0x3F, 0x3F, CR, 0x00}; //message pour interrogation capteur
char reg_capteur[4] = {0x3F, 0x3F, 0x3F, 0x3F}; // init registre des infos capteur;
int modeAuto;

/*
 * init timer in PWM moteur
 */
 void initialisations_moteur()
{
    P2SEL |= 0b00010100;
    P2SEL2 &= 0b11101011; // choix de la fonction primaire de p2.2 et p2.4 (sortie de Timer)
    P2DIR |= 0b00110110; // definition de p2.2 et p2.4  && p2.1 et p2.5 en tant que sortie
    P1DIR |= 0b01000001; // configuration de p1.0 et p1.6 en sortie
    TA1CTL = TASSEL_2| ID_0 | MC_1; // SMCLK , diviseur à 1,  mode up
    TA1CCTL1 |= OUTMOD_7; // choix du mode de sortie 7 pour TA1.1 (p2.2)
    TA1CCTL2 |= OUTMOD_7; // choix du mode de sortie 7 pour TA1.2 (p2.4)
    TA1CCR0 = 100; // T=0,1ms
}

 /*
  * fonction avancer
  */
 void avancer()
{
    P2OUT = 0b11011111;
    TA1CCR1 = 100;
    TA1CCR2 = 100;
}

 /*
  * fonction reculer doucement
  */
 void reculer()
 {
   P2OUT = 0b11111101;
   TA1CCR1 = 20;
   TA1CCR2 = 20;
   // _delay_cycles(100000);
 }

 /*
  * Tourne a droite
  */
 void droite ()
{
    P2OUT = 0b11011111;
    TA1CCR2 = 50;
    TA1CCR1 = 0;
}

 /*
  * Tourne a gauche
  */
 void gauche ()
{
    P2OUT = 0b11011111;
    TA1CCR1 = 50;
    TA1CCR2 = 0;
}

 /*
  * Stop le robot
  */
 void stop ()
{
    TA1CCR1=0;
    TA1CCR2=0;
}

 /*
  * fonction pour deplacement autonome
  */
 void AUTO()
 {
    if ((reg_capteur[1] & 0x0F ) <=2 )
    { // capteur UltraSon de devant
        if ((reg_capteur[0] & 0x0F ) <=2 )
        {  //si obstacle a gauche
            droite ();
        }
        else if ((reg_capteur[2] & 0x0F ) <=2 )
        {
            gauche ();
        }
        else
        {
            droite ();
        }
    }
    else
    {
        avancer();
    }

    // arret d'urgence
    if (reg_capteur[3] == 0x30)// capteur infrarouge
    {
        stop();
    }
 }

/*
 * initialisation de l'UART
 */
 void InitUART(void)
{
    P1SEL |= (BIT1 | BIT2);                 // P1.1 = RXD, P1.2=TXD
    P1SEL2 |= (BIT1 | BIT2);                // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 |= UCSSEL_2;                   // SMCLK
    UCA0BR0 = 104;                          // 1MHz, 9600
    UCA0BR1 = 0;                            // 1MHz, 9600
    UCA0CTL0 &= ~UCPEN & ~UCPAR & ~UCMSB;   //parity disable, LSB first
    UCA0CTL0 &= ~UC7BIT & ~UCSPB;          //data:8bit , stop:1bit
    UCA0CTL0 &= ~(UCMODE0 | UCMODE1 | UCSYNC); // UART mode asynchronous mode
    UCA0CTL1 &= ~UCSWRST;                   // **Initialize USCI state machine**
    /* Enable USCI_A0 RX interrupt */
    IE2 |= UCA0RXIE;
}

 /*
  * envoi caractere unitaire au port serie
  */
  void TXdata( unsigned char c )
 {
     // on regarde que le buffer est disponible pour l'envoi.
     while ((IFG2&UCA0TXIFG)==0)
     {
     // buffer Tx USCI_A0 vide ?   is set when UCA0TXBUF is empty.
     }
     UCA0TXBUF = c;    // on met le caractere a envoyer dans le buffer de sortie
 }

  /*
   * envoi de chaine vers le port serie
   */
   void Send_STR_UART(const char *msg)
  {
      unsigned int i = 0;
      // on envoie les caracteres un par un au port serie
      for (i=0 ; msg[i] != 0x00 ; i++)
      {
          TXdata(msg[i]);
      }
  }

 /*
  * Init de la communication SPI (maitre)
  */
 void InitSPI(void)
 {
     P1OUT |= BIT4;     // Select SPI Slave  P1.4 Low
     P1DIR |= BIT4;     // P1.4 Output
     P1SEL |= (BIT5 | BIT6 | BIT7);  //Secondary peripheral module function is selected
     P1SEL2 |= (BIT5 | BIT6 | BIT7); //Secondary peripheral module function is selected

     UCB0CTL1 = UCSWRST;
     UCB0CTL0 |= ( UCMST | UCSYNC); //master mode , synchronous mode
     // !!! LSB first, data change on first UCLK, inactive state low
     UCB0CTL0 &= ~(UCMSB |UCCKPH | UCCKPL);

     UCB0CTL1 |= UCSSEL_2; // horloge utilise: SMCLK
     // bit clk prescaler setting (UCxxBR0 + UCxxBR1 * 256)
     UCB0BR0 = 0x02;
     UCB0BR1 = 0x00;

     UCB0CTL1 &= ~UCSWRST;    //  -- initialisation state machine USCI--
     P1OUT &= ~BIT4; // Select Device
 }

/*
 * Envoi et recois un octet en SPI
 */
 void TXRXdataSPI(unsigned char c, unsigned char *d)
 {
     //unsigned char spi_data;
     while ((UCB0STAT & UCBUSY))
     {
        // attend que USCI_SPI soit dispo.
     }
     while ((IFG2 & UCB0TXIFG)==0)
     {
        //USCI_B0 TX buffer ready
     }
     UCB0TXBUF = c; // send c over spi to slave
     while ((IFG2 & UCB0RXIFG)==0)
     {  //USCI_B0 RX buffer ready
     }
     *d = UCB0RXBUF;
 }


 /*
  * envoi et reception de data SPI
  */
 void Send_Receive_STR_SPI(const char *spi_msg, char *RXmsg)
 {
     unsigned int i = 0;
     unsigned char c_answer;
     for (i=0 ; spi_msg[i] != 0x00 ; i++)  // on envoie les caracteres un par un au port SPI
     {
         TXRXdataSPI(spi_msg[i], &c_answer);
         if (i==0)
         {
             RXmsg[0]=c_answer;
         } else
         {
             RXmsg[(i-1)]=c_answer;
         }
         __delay_cycles(200);   //attente pour laisser le temps a l'esclave de traiter les donnees
     }
 }

 /*
  * Pour affichage des info capteur vers l'utilisateur
  */
 void send_reg_Capteur_to_UART(unsigned int n_capteur)
 {
     unsigned char data = reg_capteur[n_capteur];
     if ((data >> 4) < 4)
     {
        if ((data >> 4) == 0)
        {
            Send_STR_UART("US-gauche ");
        }
        else if ((data >> 4) == 1)
        {
            Send_STR_UART("US-milieu ");
        }
        else if ((data >> 4) == 2)
        {
            Send_STR_UART("US-droite ");
        }
        else if ((data >> 4) == 3)
        {
            Send_STR_UART("InfraRouge ");
        }
        data = data & 0x0F;
        Send_STR_UART("Distance: ");
        // pour conversion hexa to string
        if (data<9)
        {
            data = data + 48;
            TXdata(data);
        } else if (data < 16 )
        {
            data = data + 55;
            TXdata(data);
        }
        Send_STR_UART("       \r \n ");
     }
     else
     {
         Send_STR_UART("--error---\r\n");
     }
 }


/*
 * reception d un caratere sur le port serie
 */
 void RXdata(unsigned char *c)
{
    // on attend la reception d'un caractere
    while ((IFG2&UCA0RXIFG)==0)
    {// buffer Rx USCI_A0 plein ?    if UCA0RXBUF has received a complete character.
    }
    *c = UCA0RXBUF;   // on recupere le caractere dans la variable c
}

/*
 * interpretation des commandes, affichage sur terminal et actions
 */
 void command( char *cmd )
{
Send_STR_UART("       \r\n");  /* retour charriot pour ne pas ecraser la commande saisie */
    if (strcmp(cmd, "AUTO") == 0)
    {
        Send_STR_UART("\r Mode autonome");
        etat=1;
    }
    else if (strcmp(cmd, "GO") == 0)
    {
        Send_STR_UART("\r Avancer\n");
        etat=2;
    }
    else if (strcmp(cmd, "ST") == 0)
    {
        Send_STR_UART("\r Stop\n");
        etat=3;
    }
    else if (strcmp(cmd, "TR") == 0)
    {
        Send_STR_UART("\r Droite\n");
        etat=4;
    }
    else if (strcmp(cmd, "TL") == 0)
    {
        Send_STR_UART("\r Gauche\n");
        etat=5;
    }
    else if (strcmp(cmd, "RSC") == 0)
    {
        Send_STR_UART("\r SCAN\n");
        etat=6;
    }
    else if (strcmp(cmd, "SSC") == 0)
    {
        Send_STR_UART("\r Stop SCAN\n");
        etat=7;
    }
    else if (strcmp(cmd, "BA") == 0)
    {
        Send_STR_UART("\r Reculer\n");
        etat=8;
    }
    else if (strcmp(cmd, "query") == 0)     // interrogation position obstacle
    {
        //Send_Receive_STR_SPI(query,reg_capteur);
        send_reg_Capteur_to_UART(0);
        send_reg_Capteur_to_UART(1);
        send_reg_Capteur_to_UART(2);
        send_reg_Capteur_to_UART(3);
        etat=0;
    }
    else if (strcmp(cmd, "h") == 0)
    {
        // affichage de l'aide
        Send_STR_UART("\r\n'AUTO': Mode autonome \n");
        Send_STR_UART("\r\n'GO'  : Avancer \n");
        Send_STR_UART("\r\n'ST'  : Arret \n");
        Send_STR_UART("\r\n'BA'  : reculer\n");
        Send_STR_UART("\r\n'TR'  : Tourner a droite \n");
        Send_STR_UART("\r\n'TL'  : Tourner a gauche \n");
        Send_STR_UART("\r\n'RSC' : Demarrage du scan \n");
        Send_STR_UART("\r\n'SSC' : Arret scan \n");
        Send_STR_UART("\r\n'query' : Interrogation capteur \n");
        Send_STR_UART("\r\n'h' : Aide\n");
        etat=0;
    }
    else
    {
        Send_STR_UART("\r\nMauvaise commande ");
        Send_STR_UART(cmd);
        Send_STR_UART("\r\nEntrez 'h' pour l'aide");
        Send_STR_UART("       \r\n>");
        etat=0;
    }
    
    switch (etat)
       {
    case 1:
        {
        Send_STR_UART("Mode autonome \n");
        char scan[6]={0x61, 0x75, 0x74, 0x6F, CR, 0x00};
        // envoi de "auto" vers le port spi pour demarrage du scan
        Send_Receive_STR_SPI(scan, reg_capteur);
        modeAuto = 1;
        }
        break;
    case 2:
        Send_STR_UART("Avancer \n");
        avancer();
        break;
    case 3:
        Send_STR_UART("Arret \n");
        stop();
        modeAuto = 0;
        break;
    case 4:
        Send_STR_UART("Tourner a droite \n");
        droite();
        break;
    case 5:
        Send_STR_UART("Tourner a gauche \n");
        gauche();
        break;
    case 6:
        {
        Send_STR_UART("Demarrage scan \n");
        char autoscan[6]={0x61, 0x75, 0x74, 0x6F, CR, 0x00};
        // envoi de "auto" vers le port spi
        Send_Receive_STR_SPI(autoscan, reg_capteur);
        }
        break;
    case 7:
        {
        Send_STR_UART("Stop scan \n");
        char stopscan[6]={0x73, 0x74, 0x6F, 0x70, CR, 0x00};
        // envoie de "stop" vers le port spi
        Send_Receive_STR_SPI(stopscan, reg_capteur);
        }
        break;
    case 8:
        Send_STR_UART("Reculer \n");
        reculer();
        break;
    default:
        break;
       }
       Send_STR_UART("        \r\n>");  /* retour charriot et affichage d un prompt */
}

 /*
  * Main
  */
void main()
{
    WDTCTL = WDTPW + WDTHOLD;   // Stop WDT
    // clock calibration verification
    if (CALBC1_1MHZ==0xFF || CALDCO_1MHZ==0xFF)
    {
    // factory calibration parameters
    DCOCTL = 0;
    }
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    //Init_IO(); // initialisation des sorties
    initialisations_moteur();
    InitUART(); //intialisation de l'UART
    InitSPI(); // initalisation SPI

    nb_car = 0;
    Send_STR_UART("MSP430 Ready !\r\n>");
    /*Affichage d'un message de fin d'initialisation et d'un prompt*/

    __bis_SR_register(GIE); // general interrupts enable

    while (1)
    {
        //actualisation registre capteur (Ultrason gauche, milieu, droite & InfraRouge)
        Send_Receive_STR_SPI(query, reg_capteur);
        __delay_cycles(50000);
        if (modeAuto==1)
        {
            AUTO();
        }
    }
}

// --------------------------- R O U T I N E S   D ' I N T E R R U P T I O N S

/* ************************************************************************* */
/* VECTEUR INTERRUPTION USCI RX                                              */
/* ************************************************************************* */
#pragma vector = USCIAB0RX_VECTOR
 /* cppcheck-suppress unusedFunction ; La fonction est appelé par l'interruption */
__interrupt void USCIAB0RX_ISR()
{
    //---------------- UART
     if ( nb_car<(CMDLEN-1) )  // si le nombre de caractere n'est pas atteind
      {
        RXdata(&c); // on recoit un caractere
        cmd[nb_car]=c; //on met ce caractere dans le tableau
        if (cmd[nb_car] == CR ) // si le caractere CR a ete trouve
        {
            cmd[nb_car]=0x00; //je remplace le CR par le marqueur de find de commande caractere
            //on execute la partie interpretation des commandes et execution des actions
            command(cmd);
            nb_car=0;  // reinit du nb de caractere recu
        }
        else if (cmd[nb_car] == BS ) // si le caractere effacement est trouvé
        {
        TXdata(c); // on renvoi le caractere au port serie (retour en arriere d'un caractere
        cmd[nb_car]=0x00; //j efface le dernier caractere
        nb_car--; // on decremente le nombre de caractere
        }
        else // pour tous les autres caracteres
        {
        TXdata(c); // on renvoi le caractere au port serie
        nb_car++; // on incremente le nombre de caractere (index tableau)
        }
      }
      else // si on a saisie trop de caractere
      {
        nb_car=0;  // reinit du nb de caractere recu
        Send_STR_UART("\r\nMauvaise commande ");  // affichage d'erreur
        Send_STR_UART("\r\nEntrez 'h' pour l'aide");
        Send_STR_UART("\r\n>");  /* retour charriot et affichage d un prompt */
      }
}
